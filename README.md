Harvest plot in R
=================

### Roderick Slieker, r.slieker at vumc.nl

Required packages
-----------------

    library(ggplot2)
    library(rio)

    source("Code/Harvest_plot.R")

Rationale
---------

There is currently no R implementation of the Harvest Plot, see figure
below. This was published in BMC Medical Research Methodology 2008, 8:8.

![](https://media.springernature.com/full/springer-static/image/art%3A10.1186%2F1471-2288-8-8/MediaObjects/12874_2007_Article_244_Fig2_HTML.jpg)

Data requirements
-----------------

The data has several mandatory columns:

-   Split by columns (`colSplit`)

-   Split by rows (`rowSplit`)

-   Width of the bars (`Width`), with absolute values

-   Height of bars (`Height`), with options ‘High’ or ‘Low’

-   Bars are coloured based on `Colour` column as groups, colours can be
    supplied in the colour argument of the plot function

-   References below bars can be supplied via the `Ref` column as text
    or numerical values

Note: if for example height should be fixed to all high, just add column
with only ‘High’.

Example
-------

### Load data

    example.data <- import("Data/ExampleData.xlsx")

### Make plot

This uses the following arguments: data, colSplit, rowSplit, rowOrder,
colOrder and cols. rowOrder and colOrder are used to reorganise the
order of the rows or columns. Colours can be provided via `cols`

    p1 <- getPlot(data = example.data, colSplit = "Finding", rowSplit = "Type", 
      rowOrder = c("Age","Education","Gender","Income","Occupation"),
      colOrder=c("Negative gradient","No gradient", "Positive gradient"),
      colors = c("#132B41","#009AC7"), scale=FALSE)

### Plot

    print(p1)

![](README_files/figure-markdown_strict/unnamed-chunk-5-1.png)

### Export

    jpeg(filename = "Test plot.jpeg", width =10, height=6, res=300, units = "in")
    p1
    dev.off()

    ## quartz_off_screen 
    ##                 2

    pdf("Test plot.pdf", width =10, height=6)
    p1
    dev.off()

    ## quartz_off_screen 
    ##                 2
